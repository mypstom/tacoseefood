$(document).ready(()=>{
    var socket = io('http://192.168.43.105:3000');
    var queue = new createjs.LoadQueue();
    queue.installPlugin(createjs.Sound);
    queue.on("complete", handleComplete, this);
    queue.loadFile({id:"bg", src:"music/bg.mp3"});
    queue.loadFile({id:"cheer", src:"music/cheer.mp3"});
    queue.loadFile({id:"over", src:"music/over.mp3"});
    queue.loadFile({id:"notyet", src:"music/notyet.mp3"});
    queue.loadFile({id:"click", src:"music/click.mp3"});
    // queue.loadManifest([
    //     {id: "myImage", src:"path/to/myImage.jpg"}
    // ]);
    function handleComplete() {
        socket.emit('endBake');
        loopBg();
        function loopBg(){
            let bgPlayer = createjs.Sound.play("bg");
            bgPlayer.volume = 0.3;
            bgPlayer.on("complete", ()=>{
                loopBg();
            }, this);
        };
        socketEvents();
        // var image = queue.getResult("myImage");
        // document.body.appendChild(image);
        $(".select-food").fadeIn();
        animateThings();
        clickEvents();
    }

    let selectItem = 0;
    let fireworksLoopId;
    function clickEvents(){
        $(".candy-collider").on("click", function(){
            createjs.Sound.play("click");
            $(".check").hide();
            $(this).children(".check").fadeIn(350);
            selectItem = $(this).attr("id").split("-")[1];
            console.log(selectItem);
        });

        $(".sent").on("click", function(){
            createjs.Sound.play("click");
            if(selectItem!=0){
                goToBake();
            }
        });

        $(".more").on("click", ()=>{
            createjs.Sound.play("click");
            clearInterval(fireworksLoopId);
            $(".word-3").fadeOut(500);
            $(".more").animate({
                opacity: 0,
                top: "20%"
            }, 1500);
            $(".taco-food").animate({
                bottom: "-126%",
            }, 1500, ()=>{
                $(".taco-food").hide();
                $(".baking").hide();
                $(".select-food").show();
                $(".menu").animate({
                    bottom: "-11%",
                }, 1500, function() {
                    $(".sent").fadeIn(350);
                    $(".word-1").fadeIn(350);
                });
                selectItem = 0;
                totalBakeTime = 0;
                $(".check").hide();
                $(".candy-collider").show();
                $(".net").attr("src", "images/net.png");
                $(".fire").show();
            });

        });

        let totalBakeTime = 1;
        let bakeLimit = [18,33,43,63];
        function goToBake(){
            socket.emit('startBake');
            createjs.Sound.play("click");
            $(".candy-collider").fadeOut(350);
            $(".sent").fadeOut(350);
            $(".word-1").fadeOut(500);
            $(".menu").animate({
                bottom: "-110%",
            }, 1500, function() {
                $(".select-food").hide();
                $(".word-2").fadeIn(500);
                $(".baking").show();
                $(".taco-food").show();
                $(".taco-food").animate({
                    bottom: "-26%",
                }, 1500, ()=>{
                    let bakeLoopId = setInterval(()=>{
                        console.log(totalBakeTime);
                        
                        if(totalBakeTime>=bakeLimit[selectItem-1]){
                            socket.emit('endBake');
                            createjs.Sound.play("over");
                            console.log("烤熟了！");
                            clearInterval(bakeLoopId);
                            $(".word-3").fadeIn(500);
                            $(".word-2").hide();
                            $(".more").animate({
                                opacity: 1,
                                top: "40%"
                            }, 1500);

                            $(".fire").hide();
                            $(".fireworks-1").fadeIn(200);
                            $(".fireworks-2").fadeIn(200);
                            $(".fireworks-3").fadeIn(200);
                            $(".net").attr("src", "images/netOver.png");
                            fireworksLoopId = setInterval(makeFirework, 4000);
                        }
                        if(totalBakeTime%10==0 && !isCheckingFinish){
                            checkIsFinished();
                            totalBakeTime++;
                        }

                        if(!isCheckingFinish){
                            totalBakeTime++;
                        }
                    }, 1000);
                });
            });
        }
    }
    var isCheckingFinish = false;
    function checkIsFinished(){
        isCheckingFinish = true;
        socket.emit('endBake');
        setTimeout(makeBakeSound, 1500);
    }

    function makeBakeSound(){
        let notyet = createjs.Sound.play("notyet");
        notyet.volume = 2;
        setTimeout(()=>{
            socket.emit('startBake');
            isCheckingFinish = false;
        },1500);
    }


    function socketEvents(){

        socket.emit('msg',"OAO!!");
        socket.on('chat', function(msg){
            console.log("OAO");
        });
    
        $(".up").on("click", function(){
            socket.emit('startBake');
        });
    
        $(".down").on("click", function(){
            socket.emit('endBake');
        });
    }
    function makeFirework(){
        //Math.floor(Math.random()*50);
        for(let i=1;i<4;i++){
            $(".fireworks-"+i).show();
            $(".fireworks-"+i).css("top", Math.floor(Math.random()*60+20+"%"));
            $(".fireworks-"+i).css("left", Math.floor(Math.random()*60+20)+"%");
        }
        $(".fireworks-1").animate({
            opacity: 1,
        }, 1500, function() {
            $(".fireworks-1").animate({
                opacity: 0,
            }, 1000); 
        });

        $(".fireworks-2").animate({
            opacity: 1,
        }, 1500, function() {
            $(".fireworks-2").animate({
                opacity: 0,
            }, 1000); 
        });

        $(".fireworks-3").animate({
            opacity: 1,
        }, 1500, function() {
            $(".fireworks-3").animate({
                opacity: 0,
            }, 1000,function(){
                for(let i=1;i<4;i++){
                    $(".fireworks-"+i).show();
                    $(".fireworks-"+i).css("top", Math.floor(Math.random()*60+20+"%"));
                    $(".fireworks-"+i).css("left", Math.floor(Math.random()*60+20)+"%");
                }
            }); 
        });

    }

    function animateThings(){
        fireLoop();
        word2Loop();
        tacoHandLoop();
        function fireLoop(){
            $(".fire").animate({
                opacity: 1,
            }, 1500, function() {
                $(".fire").animate({
                    opacity: 0.6,
                }, 1500, function() {
                    fireLoop();
                }); 
            });
        }

        function word2Loop(){
            $(".word-2").animate({
                opacity: 1,
            }, 1500, function() {
                $(".word-2").animate({
                    opacity: 0,
                }, 1500, function() {
                    word2Loop();
                }); 
            });
        }

        function tacoHandLoop(){
            // Math.floor(Math.random()*60+20+"%")
            var tl = new TimelineMax({repeat:-1});
            tl.staggerTo($(".taco-hand"),2,{y:-100},0.2);
            tl.staggerTo($(".taco-hand"),2,{y:0},0.2);
        }
        

        $(".menu").animate({
            bottom: "-11%",
        }, 1500, function() {
            $(".sent").fadeIn(350);
            $(".word-1").fadeIn(350);
        });


        
    }


});

var soundID = "bg";

function playSound () {
    createjs.Sound.play(soundID);
}