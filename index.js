var five = require("johnny-five");
var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.use(express.static('public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var servoRaise;
var servoRotate;

var board = new five.Board();

board.on("ready", function() {
  servoRaise = new five.Servo.Continuous(10);
  servoRotate = new five.Servo(9);
  
  //預設是舉起
  servoRotate.stop();
  servoRaise.center();

});

io.on('connection', function(socket){
  console.log('a user connected');
  
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('startBake', function(msg){
    servoRaise.ccw();
    servoRotate.sweep();
    console.log("把手手放下去");
  });

  socket.on('endBake', function(msg){
    servoRotate.stop();
    servoRaise.center();
    console.log("把手手拿起來");
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});