var five = require("johnny-five");
var keypress = require("keypress");

keypress(process.stdin);

var board = new five.Board();
board.on("ready", function() {


  var servoRaise = new five.Servo({
    pin: 10, 
    range: [ 45, 135 ]
  });

  var servoRotate = new five.Servo(9);
  servoRotate.move(0);
  process.stdin.resume();
  process.stdin.setEncoding("utf8");
  process.stdin.setRawMode(true);

  process.stdin.on("keypress", function(ch, key) {

    if (!key) {
      return;
    }

    if (key.name === "q") {
      console.log("Quitting");
      process.exit();
    } else if (key.name === "up") {
      console.log("CW");
      servoRaise.min();

    } else if (key.name === "down") {
      console.log("Down");
      //servoRotate.sweep();
      servoRaise.max();
    } else if (key.name === "space") {
      console.log("Up");
      servoRaise.center();
      servoRotate.move(0);
    }
  });
  
  

  
});